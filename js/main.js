jQuery(document).ready(function () {
  jQuery('body').removeClass('loading')
      setTimeout(function () {
        jQuery('body').css('opacity', '1')
      }, 100);

  jQuery(window).scroll(hideBlock);
  function hideBlock() {
    jQuery('.menu__popup').removeClass('active');
    jQuery('.navigation__logo_vector').removeClass('d-none');
    jQuery('.menu__burger').removeClass('menu__burger--active')
  }

  jQuery(".menu__burger").click(function (e) {
    jQuery('.menu__popup').toggleClass('active');
    jQuery('.navigation__logo_vector').toggleClass('d-none');
    jQuery('.menu__burger').toggleClass('menu__burger--active')
    e.stopPropagation();
  });
  jQuery('.menu__popup').click(function (e) {
    e.stopPropagation();
  });
  jQuery(document).click(function () {
    jQuery('.menu__popup').removeClass('active');
    jQuery('.navigation__logo_vector').removeClass('d-none');
    jQuery('.menu__burger').removeClass('menu__burger--active')
  });

  jQuery('.menu__popup').on('click', function () {
    jQuery('.menu__burger').removeClass('menu__burger--active');
    jQuery('.navigation__logo_vector').removeClass('d-none');
    jQuery('.menu__popup').removeClass('active');
  });

  jQuery('.menu__popup_link').on('click', function (e) {
    e.preventDefault;
    jQuery('.menu__burger').removeClass('menu__burger--active');
    jQuery('.navigation__logo_vector').removeClass('d-none');
    jQuery('.menu__popup').removeClass('active');
  });
jQuery('.signUp').click(function () {
  jQuery('.sign-up').addClass('active');
  jQuery('.modal__block_shadow').addClass('active');

  jQuery('.modal__block_shadow').click(function () {
    jQuery('.sign-up').removeClass('active');
    jQuery('.modal__block_shadow').removeClass('active');
  });
});
jQuery('.signIn').click(function () {
    jQuery('.sign-up').removeClass('active');
    jQuery('.sign-in').addClass('active');
    jQuery('.modal__block_shadow').addClass('active');

    jQuery('.modal__block_shadow').click(function () {
      jQuery('.sign-in').removeClass('active');
      jQuery('.modal__block_shadow').removeClass('active');
    });
  });

  jQuery('.createAccount').click(function () {
    jQuery('.sign-in').removeClass('active');
    jQuery('.sign-up').addClass('active');
    jQuery('.modal__block_shadow').addClass('active');

    jQuery('.modal__block_shadow').click(function () {
      jQuery('.sign-up').removeClass('active');
      jQuery('.modal__block_shadow').removeClass('active');
    });
  });

  jQuery('.forgotPass').click(function () {
    jQuery('.sign-in').removeClass('active');
    jQuery('.recovery').addClass('active');
    jQuery('.modal__block_shadow').addClass('active');

    jQuery('.modal__block_shadow').click(function () {
      jQuery('.recovery').removeClass('active');
      jQuery('.modal__block_shadow').removeClass('active');
    });
  });

  jQuery('.backToSingIn').click(function () {
    jQuery('.recovery').removeClass('active');
    jQuery('.sign-in').addClass('active');
    jQuery('.modal__block_shadow').addClass('active');

    jQuery('.modal__block_shadow').click(function () {
      jQuery('.sign-in').removeClass('active');
      jQuery('.modal__block_shadow').removeClass('active');
    });
  });
  jQuery('#qr-code').qrcode({
    text: "http://torch.technology",
    width: 121,
    height: 121,
    background: "#ffffff",
    foreground: "#000000"
  });

});

